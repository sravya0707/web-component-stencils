import { Config } from '@stencil/core';
import { sass } from '@stencil/sass';
import { reactOutputTarget } from '@stencil/react-output-target';

export const config: Config = {
  namespace: 'web-components-lib-test',
  bundles: [{ components: ['rxss-accumlator'] }, { components: ['my-component'] }],
  outputTargets: [
    reactOutputTarget({
      componentCorePackage: 'web-components-lib-test',
      proxiesFile: '../web-components-lib-test-react/src/index.ts',
      includeDefineCustomElements: true,
    }),
    {
      type: 'dist',
      esmLoaderPath: '../loader',
    },
    {
      type: 'dist-custom-elements-bundle',
    },
    {
      type: 'docs-readme',
    },
    {
      type: 'www',
      serviceWorker: null, // disable service workers
    },
  ],
  testing: {
    coverageDirectory: './testReports',
    coverageReporters: ['html', 'text'],
    coverageThreshold: {
      global: {
        branches: 95,
        functions: 95,
        lines: 95,
        statements: 95,
      },
    },
    browserArgs: ['--no-sandbox', '--disable-setuid-sandbox'],
  },
  plugins: [sass()],
};
