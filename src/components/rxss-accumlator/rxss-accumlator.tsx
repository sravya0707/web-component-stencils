import { Component, Host, h, Prop } from '@stencil/core';
import { percentage } from '../../utils/utils';

@Component({
  tag: 'rxss-accumlator',
  styleUrl: 'rxss-accumlator.scss',
  shadow: true,
})
export class RxssAccumlator {
  // Max Value
  @Prop() maxValue: number;

  //  Current Value
  @Prop() currentValue: number;

  //  Fill
  @Prop() colorFill: string;

  percentage: any;

  render() {
    this.percentage = percentage(this.maxValue, this.currentValue);
    const styles = { '--percentage': this.percentage, '--fill': this.colorFill };
    return (
      <Host>
        <span>
          <div class="semi-donut margin" style={styles}>
            ${this.currentValue}
          </div>
        </span>
      </Host>
    );
  }
}
