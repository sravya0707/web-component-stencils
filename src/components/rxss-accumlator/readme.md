# rxss-accumlator



<!-- Auto Generated Below -->


## Properties

| Property       | Attribute       | Description | Type     | Default     |
| -------------- | --------------- | ----------- | -------- | ----------- |
| `colorFill`    | `color-fill`    |             | `string` | `undefined` |
| `currentValue` | `current-value` |             | `number` | `undefined` |
| `maxValue`     | `max-value`     |             | `number` | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
