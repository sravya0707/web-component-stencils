import { newE2EPage } from '@stencil/core/testing';

describe('rxss-accumlator', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<rxss-accumlator></rxss-accumlator>');

    const element = await page.find('rxss-accumlator');
    expect(element).toHaveClass('hydrated');
  });
});
