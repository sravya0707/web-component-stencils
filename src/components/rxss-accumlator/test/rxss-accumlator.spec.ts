import { newSpecPage } from '@stencil/core/testing';
import { RxssAccumlator } from '../rxss-accumlator';

describe('rxss-accumlator', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [RxssAccumlator],
      html: `<rxss-accumlator></rxss-accumlator>`,
    });
    expect(root).toEqualHtml(`
      <rxss-accumlator>
        <mock:shadow-root>
        <span>
        <div class="semi-donut margin" style="--percentage: NaN;">
        $
        </div>
                
             </span>
        </mock:shadow-root>
      </rxss-accumlator>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [RxssAccumlator],
      html: `<rxss-accumlator max-value="10000" current-value="500" color-fill="#007DAB"></rxss-accumlator>`,
    });
    expect(root).toEqualHtml(`
    <rxss-accumlator max-value="10000" current-value="500" color-fill="#007DAB">
        <mock:shadow-root>
        <span>
        <div class="semi-donut margin" style="--percentage:5; --fill:#007DAB;">
        $500
        </div>
        </span>
        </mock:shadow-root>
        </rxss-accumlator>
    `);
  });
});
