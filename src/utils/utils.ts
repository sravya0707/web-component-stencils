export function format(first: string, middle: string, last: string): string {
  return (first || '') + (middle ? ` ${middle}` : '') + (last ? ` ${last}` : '');
}

export function percentage(maxValue: number, currentValue: number) {
  const percentage = (currentValue / maxValue) * 100;
  return percentage;
}
